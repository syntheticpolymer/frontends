# Front-ends for evil sites/platforms

## ::Youtube::
- Sources: 
- https://github.com/iv-org/invidious
- https://sr.ht/~cadence/tube/ [2]
- https://github.com/TeamPiped/Piped [3]
---
- Instances:
- https://invidious.tube
- https://invidious.kavin.rocks
- https://invidious.himiko.cloud
- https://invidious.namazso.eu
- [2] https://tube.cadence.moe 
- [3] https://piped.kavin.rocks

## ::Twitter::
- Source: https://github.com/zedeus/nitter
---
- Instances:
- https://nitter.nixnet.services 
- https://nitter.tedomum.net
- https://nitter.pussthecat.org
- https://nitter.kavin.rocks
- https://nitter.himiko.cloud


## ::Instagram::
- Source: https://sr.ht/~cadence/bibliogram/
---
- Instances:
- https://bibliogram.pussthecat.org
- https://bibliogram.nixnet.services


## ::Reddit::
Sources:
- https://github.com/spikecodes/libreddit
- https://codeberg.org/teddit/teddit (2)
---
- Instances:
- https://libredd.it (official) 
- https://libreddit.kavin.rocks
- https://libreddit.himiko.cloud
- [2] https://teddit.net


## ::Google Translate::
- Sources:
- https://sr.ht/~yerinalexey/gtranslate/ (1)
- https://git.sr.ht/~metalune/simplytranslate_web (2)
---
- Instances:
- [1] https://gtranslate.metalune.xyz
- [2] https://translate.metalune.xyz


## ::Google Search Engine::
- Source: https://github.com/benbusby/whoogle-search
---
- Instances:
- https://whoogle-search.zeet.app/
- https://whoogle.himiko.cloud/ 
- https://whoogle.kavin.rocks/
- https://whoogle.sdf.org
